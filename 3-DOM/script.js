'use strict';

const body = document.querySelector( 'body' );
const div = document.createElement( 'div' );
const p = document.createElement( 'p' );

setInterval( () => {
    body.appendChild(div).appendChild(p).innerHTML = new Date().toTimeString().split( ' ' )[0];
} );